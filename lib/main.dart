import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
  home: Home(),
));

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CAM_SHOP'),
        centerTitle: true,
        backgroundColor: Colors.deepOrange,
      ),
      // body: Center(
      //   /* Example for Text */
      //   // child: Text(
      //   //   'Hello all customers',
      //   //   style: TextStyle(
      //   //     fontSize: 20.0,
      //   //     fontWeight: FontWeight.bold,
      //   //     letterSpacing: 2.0,
      //   //     color: Colors.grey[600],
      //   //     fontFamily: 'IndieFlower',
      //   //   ),
      //
      //   /* Example for Image */
      //   // child: Image(
      //   //   image: NetworkImage('https://i.pinimg.com/originals/04/8e/ba/048eba948cea3a92cd40ba3e003b2538.jpg'),
      //   //   image: AssetImage('assets/01.jpg'),
      //   // ),
      //   // child: Image.asset('assets/01.jpg'),
      //   // child: Image.network('https://i.pinimg.com/originals/04/8e/ba/048eba948cea3a92cd40ba3e003b2538.jpg'),
      //
      //   /* Example for Icon widget */
      //   // child: Icon(
      //   //   Icons.airport_shuttle,
      //   //   color: Colors.lightBlue,
      //   //   size: 50.0,
      //   // ),
      //   // child: RaisedButton(
      //   //   onPressed: () {},
      //   //   child: Text('Click me'),
      //   //   color: Colors.lightBlue,
      //   // ),
      //   // child: FlatButton(
      //   //   onPressed: () {
      //   //     print('you clicked me');
      //   //   },
      //   //   child: Text('Click me'),
      //   //   color: Colors.lightBlue,
      //   // ),
      //   // child: RaisedButton.icon(
      //   //     onPressed:() {},
      //   //     icon: Icon(
      //   //         Icons.mail
      //   //     ),
      //   //     label: Text('mail me'),
      //   //     color: Colors.amber,
      //   // ),
      //   // child: IconButton(
      //   //   onPressed: () {
      //   //     print('you clicked ma');
      //   //   },
      //   //   icon: Icon(Icons.alternate_email),
      //   //   color: Colors.amber,
      //   // ),
      // ),

      // body: Container(
      //   // padding: EdgeInsets.all(20.0),
      //   // padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 10.0),
      //   padding: EdgeInsets.fromLTRB(10.0, 20.0, 30.0, 40.0),
      //   margin: EdgeInsets.all(30.0),
      //   color: Colors.grey[400],
      //   child: Text('Hello'),
      // ),

      // This padding widget will can not use margin //
      // body: Padding(
      //   padding: EdgeInsets.all(90.0),
      //   child: Text('Hello'),
      // ),

      // body: Row(
      //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      //   crossAxisAlignment: CrossAxisAlignment.start,
      //   children: <Widget>[
      //     Text('Hello, world'),
      //     FlatButton(
      //       onPressed: () {},
      //       color: Colors.amber,
      //       child: Text('click me'),
      //     ),
      //     Container(
      //       color: Colors.cyan,
      //       padding: EdgeInsets.all(30.0),
      //       child: Text('inside container'),
      //     )
      //   ],
      // ),

      // body: Column(
      //   mainAxisAlignment: MainAxisAlignment.end,
      //   crossAxisAlignment: CrossAxisAlignment.end,
      //   children: <Widget>[
      //     // Row(
      //     //   children: <Widget>[
      //     //     Text('hello'),
      //     //     Text('world')
      //     //   ],
      //     // ),
      //     Container(
      //       padding: EdgeInsets.all(30.0),
      //       color: Colors.pinkAccent,
      //       child: Text('two'),
      //     ),
      //     Container(
      //       padding: EdgeInsets.all(20.0),
      //       color: Colors.cyan,
      //       child: Text('one'),
      //     ),
      //     Container(
      //       padding: EdgeInsets.all(40.0),
      //       color: Colors.amber,
      //       child: Text('three'),
      //     ),
      //   ],
      // ),\

      body: Row(
        children: <Widget>[
          Expanded(
              child: Image.asset('assets/03.jpg'),
            flex: 3,
          ),
          Expanded(
            flex: 3,
            child: Container(
              padding: EdgeInsets.all(30.0),
              color: Colors.cyan,
              child: Text('1'),
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(
              padding: EdgeInsets.all(30.0),
              color: Colors.pinkAccent,
              child: Text('2'),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              padding: EdgeInsets.all(30.0),
              color: Colors.amber,
              child: Text('3'),
            ),
          )
        ],
      ),

      floatingActionButton: FloatingActionButton(
        onPressed: () {

        },
        child: Text('Click'),
        backgroundColor: Colors.deepOrange,
      ),
    );
  }
}
